#!/usr/bin/env bash

envsubst </etc/metricbeat/metricbeat.yml >out.yml && mv out.yml /etc/metricbeat/metricbeat.yml
service metricbeat start

/usr/bin/storm-daemon "$@"
