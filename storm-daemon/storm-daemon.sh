#!/usr/bin/env bash

# Enter posix mode for bash
set -o posix
set -e

envsubst < /mnt/conf/storm.yaml > /conf/_storm.yaml || echo "WARN: error substituting /mnt/conf/storm.yaml"
envsubst < /conf/_storm.yaml > /conf/storm.yaml || echo "WARN: error substituting /conf/_storm.yaml"

echo "INFO: Final config (/conf/storm.yaml):"
cat /conf/storm.yaml
echo "____________________"

if [ "$1" == 'bash' ]; then
  exec "$@";
fi

if ! [ "$1" == 'storm' ]; then
  echo "ERROR: Invalid usage!" >&2;
  echo "Expected Usage: storm (logviewer|nimbus|drpc|supervisor|ui) [<storm_parameters>]" >&2;
  echo "Actual Usage:   $*" >&2;
  exit 2;
fi

# Create supervisor configurations for Storm daemons
echo "[supervisord]
nodaemon=true
user=storm" > "$SUPERVISOR_CONFIG"
echo "Create supervisord configuration for 'storm $1'"
cat "$STORM_CONFIG_TEMPLATE" | sed s,%command%,"$*",g | sed s,%daemon%,"$2",g >> "$SUPERVISOR_CONFIG"

echo "____________________"
echo "Running: supervisord -c $SUPERVISOR_CONFIG"
echo "-----------"
cat "$SUPERVISOR_CONFIG"
echo "____________________"
supervisord -c "$SUPERVISOR_CONFIG"
