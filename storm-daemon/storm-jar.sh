#!/bin/bash
# example parameter: WordCountSeal 100

pwd_os_agnostic=$(pwd -W || pwd)
echo "dirname:         $(dirname "$0")"
echo "pwd_os_agnostic: $pwd_os_agnostic"
cd "$pwd_os_agnostic" || (echo "cd failed..." && exit 2)

wget -O storm.jar "https://gitlab.com/ffhs-sterchi/bt/images/-/package_files/$PACKAGE_FILE/download"
echo "storm jar $pwd_os_agnostic/storm.jar ch.novarx.ffhs.topology.$*"
storm jar storm.jar "ch.novarx.ffhs.topology.$*"
